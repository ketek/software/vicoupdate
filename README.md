# VICOUpdate

KETEK offers a user-friendly graphical interface tool designed for the seamless update of all firmware-related components associated with the hardware from the 3rd generation of products. Additionally, an efficient command-line interface is provided for automated update processes. VICOUpdate is an integral part of the comprehensive software package offered by KETEK, accessible through the [Installer](https://gitlab.com/ketek/software/installer/-/releases/permalink/latest).

This repository is only used to give easy access to the update packages used within VICOUpdate.

## Documentation

Thorough, step-by-step guidance on utilizing VICOUpdate for firmware updates, including detailed CLI commands, can be found in the [product-specific manuals](https://www.ketek.net/downloads).

## Update Packages

To update a device, it is essential to utilize a designated VICOUpdate package with the `*.vup` file extension, supplied by KETEK. This package encompasses up to three encrypted files:

- FPGA firmware file (applicable to digital versions only)
- MCU firmware file
- Whitelist file containing details about the specific device to which this update package pertains.

Select **Deploy > Package Registry** on the left sidebar to access all [VICOUpdate packages](https://gitlab.com/ketek/software/vicoupdate/-/packages) including the change history.

### Naming Convention

- For an update package with only FPGA firmware and whitelist file:  
  `<FPGA Firmware Article Number> + '_' + <FPGA Firmware Version>`  
  (e.g., `ESW-XV3.0-FPGA-01_1.0.0.0`).

- For an update package with only MCU firmware and whitelist file:  
  `<MCU Firmware Article Number> + '_' + <MCU Firmware Version>`  
  (e.g., `ESW-XV3.0-MCU-01_2.0.0.0`).

- For an update package containing both FPGA and MCU firmware and whitelist file:  
  `<FPGA Firmware Article Number> + '_' + <FPGA Firmware Version> + '-' + <MCU Firmware Article Number> + '_' + <MCU Firmware Version>`  
  (e.g., `ESW-XV3.0-FPGA-01_1.0.0.0-ESW-XV3.0-MCU-01_2.0.0.0`).

## Support

If you have any questions, feedback, or encounter issues related to this repository, please feel free to reach out to us via email or phone:

KETEK GmbH  
Hofer Str. 3  
81737 Munich  
Germany

Phone: +49 (0) 89 673467 70  
E-Mail: <info@ketek.net>  
Homepage: [www.ketek.net](https://www.ketek.net)
